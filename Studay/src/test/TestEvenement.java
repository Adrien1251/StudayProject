package test;
import java.util.Date;

import calend.Evenement;

public class TestEvenement {

	@Test
	public void testGetEvent() { 
		//test de getEvent avec la date 18/07/1950
		Evenement event1 = new Evenement (18, 07, 1950);
		@SuppressWarnings("deprecation")
		Date date1 = new Date (1950, 07, 18);
		assertTrue(date1.equals(event1.getEvent()));
		
		//test de getEvent avec la date 27/05/1999
		Evenement event2 = new Evenement (27, 05, 1999);
		@SuppressWarnings("deprecation")
		Date date2 = new Date (1999, 05, 27);
		assertTrue(date2.equals(event2.getEvent()));
		
		//test de getEvent avec la date 03/02/1901
		Evenement event3 = new Evenement (03, 02, 1901);
		@SuppressWarnings("deprecation")
		Date date3 = new Date (1901, 02, 03);
		assertTrue(date3.equals(event3.getEvent()));
	}
	
}
