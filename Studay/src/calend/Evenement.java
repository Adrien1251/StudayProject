package calend;
//imports

public class Evenement {

	//attributs
	private String nom;
	private int heure = 8;
	private int minute = 0;

	public String getNom(){
		return nom;
	}
	public String getHeure(){
		return heure+"h"+minute;
	}
	
	public Evenement (String nom){
		this.nom = nom;
	}
	public Evenement (String nom, int heure, int minute){
		this(nom);
		if (heure<=23 && heure >=0 && minute<=59 && minute >=0){
			this.heure = heure;
			this.minute = minute;
		} else { 
			System.out.println("heure saisie invalide, ajustée à 8h00");
		}
	}
}
