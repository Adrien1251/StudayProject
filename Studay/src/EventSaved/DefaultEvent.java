package EventSaved;


public enum DefaultEvent {
	
	JOURDELAN("Jour de l'An"), SAINTVALENTIN("Saint Valentin"), PRINTEMPS("Printemps"), FETEDUTRAVAIL("Fête du Travail"), ARMISTICE1945("Armistice 1945"), ETE("Eté"), FETENATIONALE("Fête Nationale"), 
	ASSOMPTION("Assomption"), AUTOMNE("Automne"), HALLOWEEN("Halloween"), TOUSSAINT("Toussaint"), ARMISTICE1918("Armistice 1918"), SAINTECATHERINE("Sainte Catherine"), SAINTNICOLAS("Saint Nicolas"), HIVER("Hiver"), NOEL("Noël");
	
	private String nom;
	
	DefaultEvent(String nom){
		this.nom=nom;
	}

	public String getNom() {
		return nom;
	}
	
}


