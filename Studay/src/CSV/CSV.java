package CSV;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import calend.EasyDate;

public class CSV {
	public void ajouterDate(EasyDate date){
		File fichier = new File("fichier/dateCalendrier.csv");
			FileWriter writer;
			try {
				writer = new FileWriter(fichier);
				writer.write(date.getJour()+"/"+date.getMois()+"/"+date.getAnnee()+", UN NOM");
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	public void ajouterDate(ArrayList<EasyDate> date){
		File fichier = new File("fichier/dateCalendrier.csv");
			FileWriter writer;
			try {
				writer = new FileWriter(fichier);
				for(EasyDate date1 : date){
					writer.write(date1.getJour()+"/"+date1.getMois()+"/"+date1.getAnnee());
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

	}
}
